
-- Author:              A. Genson
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              anthony.genson@outlook.com
--
-- Last modified:       13.9.2020
--
-- Filename:            wtp.adb

pragma SPARK_Mode (On);
package body WTP
with
   Refined_State => (State => Prev_Sensor_Reading)
is
   -- Previous value for the sensor reading
   -- Used for checking if water level decreases when the alarm just raised
   Prev_Sensor_Reading: Integer := 1000;

   -- WTP Control procedure responsible for overall control of the WTP System
   procedure Control
   with
      Refined_Global => (In_Out => (Prev_Sensor_Reading, Alarm.State, Drain.State),
                           Input => (Sensors.State, Console.State)),
      Refined_Depends => (Prev_Sensor_Reading => Sensors.State,
                           Drain.State => (Prev_Sensor_Reading, Sensors.State, Console.State, Alarm.State, Drain.State),
                           Alarm.State => (Prev_Sensor_Reading, Sensors.State, Console.State, Alarm.State, Drain.State))
   is
      Sensor_Reading: Integer;
   begin
      -- Reading recent sensor majority value
      Sensor_Reading := Sensors.Read_Sensor_Majority;

      if Alarm.Enabled then
         -- Checks for when to disable the alarm / close the drain
         if GetLevel(Sensor_Reading) = Normal then Reset;
         elsif GetLevel(Sensor_Reading) /= High and Console.Reset_Enabled then Reset;
         elsif GetLevel(Sensor_Reading) = High then
            if Sensor_Reading < Prev_Sensor_Reading and (not Drain.Openned) then Reset;
            -- If the alarm just raised, the water level is still high, so we open the drain
            elsif (not Drain.Openned) then Drain.Open;
            end if;
         end if;
      elsif GetLevel(Sensor_Reading) = Low or GetLevel(Sensor_Reading) = High then Alarm.Enable;
      end if;

      -- Update WTP state
      Prev_Sensor_Reading := Sensor_Reading;
   end Control;

   -- Disable the Alarm and Close the drain
   procedure Reset
   is
   begin
      if Alarm.Enabled then
         Alarm.Disable;
      end if;

      if Drain.Openned then
         Drain.Close;
      end if;
   end Reset;

   -- Format water level from a given value
   function GetLevel(Value: Integer) return Water_Levels
   with
      Refined_Depends => (GetLevel'Result => Value)
   is
   begin
      if (Value >= 100) and (Value < 2000) then return Normal;
      elsif (Value >= 0) and (Value < 100) then return Low;
      elsif (Value >= 2000) and (Value < 2100) then return High;
      else return Undef;
      end if;
   end GetLevel;

end WTP;
