
-- Author:              A. Genson
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              anthony.genson@outlook.com
--
-- Last modified:       13.9.2020
--
-- Filename:            sensors.adb

pragma SPARK_Mode (On); 
package body Sensors
with
   Refined_State => (State => (Sensors_Value))
is
   type Sensor_Arr_Type is array (Sensor_Index_Type) of Sensor_Type;

   -- Array containing the distinct value of the 3 sensors
   -- Initialize the values at 1000 (average value - normal)
   Sensors_Value: Sensor_Arr_Type := (others => 1000);

   -- Setter for the 3 sensors value
   procedure Write_Sensors(Value_1, Value_2, Value_3: in Sensor_Type)
   with
      Refined_Global => (Output => Sensors_Value),
      Refined_Depends => (Sensors_Value => (Value_1, Value_2, Value_3))
   is
   begin
      -- We first initialize at an average value for the sensors
      Sensors_Value := (others => 1000);
      Sensors_Value(1) := Value_1;
      Sensors_Value(2) := Value_2;
      Sensors_Value(3) := Value_3;
   end Write_Sensors;

   -- Getter for a specific sensor value given its index
   function Read_Sensor(Sensor_Index: in Sensor_Index_Type) return Sensor_Type
   with
      Refined_Global  => (Input => Sensors_Value),
      Refined_Depends => (Read_Sensor'Result => (Sensors_Value, Sensor_Index))
   is
   begin
      return Sensors_Value(Sensor_Index);
   end Read_Sensor;

   -- Returns the majority of the sensors value, null otherwise
   function Read_Sensor_Majority return Sensor_Type
   with
      Refined_Global => (Input => Sensors_Value),
      Refined_Depends => (Read_Sensor_Majority'Result => Sensors_Value)
   is
   begin
      if Sensors_Value(1) = Sensors_Value(2) then return Sensors_Value(1);
      elsif Sensors_Value(1) = Sensors_Value(3) then return Sensors_Value(1);
      elsif Sensors_Value(2) = Sensors_Value(3) then return Sensors_Value(2);
      else return Sensor_Type'Last;
      end if;
   end Read_Sensor_Majority;

end Sensors;
