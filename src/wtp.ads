
-- Author:              A. Genson
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              anthony.genson@outlook.com
--
-- Last modified:       13.9.2020
--
-- Filename:            wtp.ads

pragma SPARK_Mode (On);
with Sensors, Alarm, Drain, Console;
package WTP
with
   Abstract_State => State
is
   type Water_Levels is (Low, Normal, High, Undef);

   -- WTP Control procedure responsible for overall control of the WTP System
   procedure Control
   with
      Global  => (In_Out => (State, Alarm.State, Drain.State),
                  Input => (Sensors.State, Console.State)),
      Depends => (State  => (State, Sensors.State),
                  Drain.State => (State, Sensors.State, Console.State, Alarm.State, Drain.State),
                  Alarm.State => (State, Sensors.State, Console.State, Alarm.State, Drain.State));

   -- Disable the Alarm and Close the drain
   procedure Reset;

   -- Format water level from a given value
   function GetLevel(Value: Integer) return Water_Levels
   with
      Depends => (GetLevel'Result => Value);

end WTP;
