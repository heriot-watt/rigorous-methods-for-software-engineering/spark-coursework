
-- Author:              A. Genson
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              anthony.genson@outlook.com
--
-- Last modified:       13.9.2020
--
-- Filename:            console.adb

pragma SPARK_Mode (On); 
package body Console
with
	Refined_State => (State => Reset_Value)
is
   -- Boolean representing if reset is enabled (True) or disabled (False)
   Reset_Value: Boolean := False;
   
   -- Enable the reset (set value to True)
   procedure Enable_Reset
   with
      Refined_Global => (Output => Reset_Value),
      Refined_Depends => (Reset_Value => null)
   is
   begin
      Reset_Value := True;
   end Enable_Reset;

   -- Disable the reset (set value to False)
   procedure Disable_Reset
   with
      Refined_Global => (Output => Reset_Value),
      Refined_Depends => (Reset_Value => null)
   is
   begin
      Reset_Value := False;
   end Disable_Reset;

   -- Getter for the reset state (reset enabled or disabled)
   -- Returns a Boolean
   function Reset_Enabled return Boolean
   with
      Refined_Global => (Input => Reset_Value),
      Refined_Depends => (Reset_Enabled'Result => Reset_Value)
   is
   begin
      return Reset_Value;
   end Reset_Enabled;

end Console;
