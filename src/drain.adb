
-- Author:              A. Genson
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              anthony.genson@outlook.com
--
-- Last modified:       13.9.2020
--
-- Filename:            drain.adb

pragma SPARK_Mode (On);
package body Drain
with
	Refined_State => (State => Drain_Value)
is
   -- Boolean representing drain opened (True) or closed (False)
   Drain_Value: Boolean := False;

   -- Open the drain (set value to True)
   procedure Open
   with
      Refined_Global => (Output => Drain_Value),
      Refined_Depends => (Drain_Value => null)
   is
   begin
      Drain_Value := True;
   end Open;

   -- Close the drain (set value to False)
   procedure Close
   with
      Refined_Global => (Output => Drain_Value),
      Refined_Depends => (Drain_Value => null)
   is
   begin
      Drain_Value:= False;
   end Close;

   -- Getter for the drain state (drain opened or closed)
   -- Returns a Boolean
   function Openned return Boolean
   with
      Refined_Global => (Input => Drain_Value),
      Refined_Depends => (Openned'Result => Drain_Value)
   is
   begin
      return Drain_Value;
   end Openned;

end Drain;
