
-- Author:              A. Genson
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              anthony.genson@outlook.com
--
-- Last modified:       13.9.2020
--
-- Filename:            alarm.adb

pragma SPARK_Mode (On);
package body Alarm 
with
   Refined_State => (State => (Alarm_Value, Alarm_Count))
is
   -- Boolean representing if the alarm is enabled (True) or disabled (False)
   Alarm_Value: Boolean := False;
   -- Counter value for alarm events
   Alarm_Count: Integer := 0;

   -- Enable the alarm (set value to True and increment event counter)
   procedure Enable
   with 
      Refined_Global  => (In_Out => (Alarm_Value, Alarm_Count)),
      Refined_Depends => (Alarm_Value => Alarm_Value,
                           Alarm_Count => (Alarm_Value,Alarm_Count))
   is
   begin
      if not Alarm_Value then
         Alarm_Value := True;

         if (Alarm_Count < Integer'Last) then
            Alarm_Count := Alarm_Count+1;
         end if;
      end if;
   end Enable;

   -- Disable the alarm (set value to False)
   procedure Disable
   with
      Refined_Global  => (In_Out => Alarm_Value),
      Refined_Depends => (Alarm_Value => Alarm_Value)
   is
   begin
      if Alarm_Value then
         Alarm_Value := False;
      end if;
   end Disable;

   -- Getter for the alarm state (alarm enabled or disabled)
   -- Returns a Boolean
   function Enabled return Boolean
   with 
      Refined_Global => (Input => Alarm_Value),
      Refined_Depends => (Enabled'Result => Alarm_Value)
   is
   begin
      return Alarm_Value;
   end Enabled;
   
   -- Getter for the alarm events counter state
   -- Returns an Interger
   function Alarm_Cnt_Value return Integer
   with
      Refined_Global => (Input => Alarm_Count),
      Refined_Depends => (Alarm_Cnt_Value'Result => Alarm_Count)
   is
   begin
      return Alarm_Count;
   end Alarm_Cnt_Value;

end Alarm;
